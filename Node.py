
class Node(object):
    
    def __init__(self,Name,Distance = 0,Depth = 0, Parent = None, Color = None):
        
        self._Name = Name
        self._Key = Name
        self._Children = list()
        self._Distance = Distance
        self._Parent = Parent
        self._Depth = Depth
        self._Left = None
        self._Right = None
        self._Color = Color
        
    def Add_Child(self,Child):
        
        self._Children.append(Child)
        #return self
    
    def Name(self):
        return self._Name 
    
    def Distance(self):
        return self._Distance
    
    def Key(self):
        self._Key
    
    def Depth(self):
        return self._Depth
    
    def Left(self):
        return self._Left
    
    def Right(self):
        return self._Right
    
    def Parent(self):
        return self._Parent
    
    # Boolean param allows for the names of the node to be returned.
    def Children(self,Return_Names = False):
        if (not Return_Names):
            return self._Children
        else:
            Names = list()
            for Child in self._Children:
                Names.append(Child.Name())
            
            return Names
        
    def Set_Distance(self,Value):
        
        self._Distance = Value;
        
    def Set_Depth(self,Value):
        self._Depth = Value;
        
    def SetLeft(self, node: Node):
        self._Left = node
        
    def SetRight(self, node: Node):
        self._Right = node
    
    def SetParent(self, node: Node):
        self._Parent = node
        
        
    