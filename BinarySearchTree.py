from Node import Node

class BinarySearchTree(object):
    
    def __init__(self,root: Node):
        
        self._root = root
        
    def Root(self):
        return self._root
    
    def Search(self,key):
        return None
    
    def Minimum(self):
        return None
    
    def Maximum(self): 
        return None
    
    def Predecessor(self, node: Node):
        return None
    
    def Successor(self, node: Node):
        return None
    
    def Insert(self,node: Node):
        return None
    
    def Delete(self, node: Node):
        return None
    
    def InorderTreeWalk(self):
        return None
    
    def PreOrderTreeWalk(self):
        return None
    
    def PostOrderTreeWalk(self):
        return None
    
    def Transplant(self, u: Node, v: Node):
        return None
   